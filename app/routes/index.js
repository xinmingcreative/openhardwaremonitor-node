var express = require('express');
var router = express.Router();
var os = require('os');

// get the latest data file from public/data folder
var fs = require('fs');
var {resolve, join} = require('path');

function convertFileNameToDate(file) {
  var chunks = file.split('-');
  var year = chunks[1];
  var month = chunks[2];
  var day = chunks[3].substring(0,2);
  // console.log("Convert to date: " + year+"-"+month+"-"+day);
  return new Date(year, month-1, day);
}
/**
 * Return true if date2 > date1 more than a day.
 */
function isMissingDay(date1, date2) {
  return (date2.getTime() - date1.getTime() > 1000*3600*24)
    ? true:false;
}

/**
 * Parse result from git log into array
 * where [0] is git hash and [1] is a date.
 */
function parseGitLog(result) {
  var info = [];
  var lines = result.split("\n");
  var words = lines[0].split(" ");
  info[0] = words[1].trim().substring(0,6);

  //var date = words[5]+"-"+words[2]+"-"+words[3];
  var date = lines[2].substring(6, lines[2].length-6);
  console.log(date.trim());
  info[1] = date.trim();

  return info;
}

// console.log("Environment: " + process.env);

/* GET home page. */
var counter = 0; // loading page counter
router.get('/', function(req, res, next) {
  counter++;
  console.log("Loading index " + counter);

  // always load the latest log file
  // TODO: Or should show last 7 days log?
  var latestFile = "";
  var logs = [];
  fs.readdir('public/data', function(err, list) {
    var i = 0;
    list.forEach(function(file) {
      i++;
      var index = file.search('[0-9]{4}-[0-9]{2}-[0-9]{2}');
      if(index > -1) {
        if(file > latestFile) {
          latestFile = file;
          logs.push(file);
          console.log(file);
        }
      }

      // only pass in 7 days log file
      var date1 = new Date();
      var date2 = new Date();
      if(i == list.length) {
        logs.sort();
        var lastSeven = [];
        for(var j=logs.length-1;j>=0;j--) {
          if(j==logs.length-1) {
            date1 = convertFileNameToDate(logs[j]);
          }
          date2 = convertFileNameToDate(logs[j]);
          // HACK: Do not load following if more than 1 day log file missing
          if(isMissingDay(date2, date1)) {
            break;
          }
          lastSeven.push(logs[j]);
          date1 = convertFileNameToDate(logs[j]);
          if(lastSeven.length == 7) {
            break;
          }
        }
        lastSeven.sort();

        // get git last log
        var process = require("child_process");
        var info = [];
        // git rev-parse HEAD
        try {
          var gitLastLog = process.execSync("git log -1").toString().trim();
          info = parseGitLog(gitLastLog);
        } catch(err) {
          console.log(err);
        }

        res.render('index', {
          title: 'Online Temperature Monitoring',
          computerName: os.hostname(),
          dataFile: latestFile,
          dataFiles: lastSeven,
          version: info[0],
          buildDate: info[1]
        });
      }
    });
  });

});

module.exports = router;

// retrieve pass 7 days log filename
var logs = [];
for(var i=0;i<7;i++) {
	var div = d3.select("#dataFile"+i)[0][0];
	if(div == null) {
		break;
	}
	var file = div.innerText;
	logs.push("/../data/"+file);
}

// Start rendering chart
multiCsv(logs, function(error, results) {
	if(error) {
		alert(error);
		return;
	}
	console.log("Finish merge all log files");
	chart(results);
});

// var file = "/../data/" + d3.select("#dataFile")[0][0].innerText; //'/../data/TAMBUN-GTX1070.csv'
// d3.csv(file, function(error, data) {
// 	chart(data);
// });

/**
 * Round up to closer minute in 5 minutes a tick.
 */
function roundUpToFive(minutes) {
	var roundup = minutes;
	var remainder = minutes%5;
	if(remainder < 3) {
		roundup = Math.floor(minutes/5)*5;
	} else {
		roundup = Math.ceil(minutes/5)*5;
	}
	return roundup;
}
/**
 * Return true if match with NVidia grahic card temperature header in OpenHardwareMonitor log.
 * Support maximum 10 cards in the log file.
 */
function isTemperatureHeader(value) {
	var index = value.search('/nvidiagpu/[0-9]/temperature/0');
	// TODO: Interested in temperature of Intel processor?
	if(index == -1) {
	 	index = value.search('/intelcpu/[0-9]/temperature/0');
	}
	return (index > -1) ? true:false;
}

/**
 * Merge multiple csv files into single data source.
 * Modified from https://stackoverflow.com/questions/29222662/what-is-the-best-way-to-combine-multiple-csv-files-data-input-using-d3-csv-in-d3
 */
function multiCsv(files, callback) {
  var results = [];
  var error = "";
  var filesLength = (files || []).length;
  var callbackInvoked = false;
  for (var i = 0; i < filesLength; i++) {
    (function(url) {
      d3.csv(url, function(data) {
        if (data === null) {
          error += "Error retrieving \"" + url + "\"\n";
        } else {
          results.push(data);
        }
        // all files retrieved or an error occurred
        if (!callbackInvoked && (error || results.length === filesLength)) {
          if (error) {
            callback(error, null); // might want to send partial results here
          } else {
            callback(null, d3.merge(results));
          }
          callbackInvoked = true;
        }
      });
    })(files[i]);
  }
}

function chart(data) {

	var cards = []; // interested headers
	var headers = d3.keys(data[0]);
	for(var i=0;i<headers.length;i++) {
		var header = headers[i];
		var isHeader = isTemperatureHeader(header);
		if(isHeader == true) {
			cards.push(header);
		}
	}
	for(var i=0;i<cards.length;i++) {
		console.log(cards[i]);
	}

	// This is the format our dates are in, e.g 6/10/2017 10:07:00
	var timeFormat = d3.time.format('%m/%d/%Y %H:%M:%S');
	var dates = [];
	var dateStrings = [];
	var temps = [];
	// multidimensional array for temperature data poitns
	var temperatures = new Array(cards.length);

  // start read from second row
	for(var i=1;i<data.length;i++) {
		// TODO: 1. Round up to the desired interval ie. 5 minutes.
		var dateString = data[i][''];
		//console.log("Loading data["+i+"]:"+dateString);
		if(dateString == "" || dateString == "Time") {
			continue;
		}
		// round up minute
		var minutes = roundUpToFive(dateString.substr(14,2));
		dateString = dateString.substring(0, 13);
		dateString = dateString + ":"+minutes+":00";
		//console.log(data[i]['']+"->"+dateString);
		dateStrings.push(dateString);

		// Convert date string into JS date, add it to dates array
		var d = timeFormat.parse(data[i]['']);
		var minutes = roundUpToFive(d.getMinutes());
		d.setMinutes(minutes);
		d.setSeconds(0);
		dates.push(d);
		//console.log(d);

		// Add high temperature to temps array
		temps.push(+data[i][cards[0]]); // '/nvidiagpu/0/temperature/0'
		for(var j=0;j<cards.length;j++) {
			if(i==1) {
				temperatures[j] = [];
			}
			temperatures[j].push(+data[i][cards[j]]);
		}
	}

	// data.forEach(function(d) {
	// });

	var chartWidth = 800,
	    chartHeight = 250,
	    margin = {
	    	top: 5,
	    	right: 25,
	    	bottom: 20,
	    	left: 25
	    };

	var container = d3.select('#temp-chart');

	var svg = container.append('svg')
		.attr('width', chartWidth)
		.attr('height', chartHeight);

	var defs = svg.append('defs');

	// clipping area
	defs.append('clipPath')
		.attr('id', 'plot-area-clip-path')
		.append('rect')
			.attr({
				x: margin.left,
				y: margin.top,
				width: chartWidth - margin.right - margin.left,
				height: chartHeight - margin.top - margin.bottom
			});

	// Invisible background rect to capture all zoom events
	var backRect = svg.append('rect')
		.style('stroke', 'none')
		.style('fill', '#FFF')
		.style('fill-opacity', 0)
		.attr({
			x: margin.left,
			y: margin.top,
			width: chartWidth - margin.right - margin.left,
			height: chartHeight - margin.top - margin.bottom,
			'pointer-events': 'all'
		});

	var axes = svg.append('g')
		.attr('pointer-events', 'none')
		.style('font-size', '11px');

	var chart = svg.append('g')
		.attr('class', 'plot-area')
		.attr('pointer-events', 'none')
		.attr('clip-path', 'url(#plot-area-clip-path)');





	// x scale
	var xScale = d3.time.scale()
		.range([margin.left, chartWidth - margin.right])
		.domain(d3.extent(dates));

	// Calculate the range of the temperature data
	var boundary = d3.extent(temperatures[0]);
	var maxY = boundary[1];
	var minY = boundary[0];
	for(var i=1;i<cards.length;i++) {
		boundary = d3.extent(temperatures[i]);
		if(boundary[0] < minY) {
			minY = boundary[0];
		}
		if(boundary[1] > maxY) {
			maxY = boundary[1];
		}
	}
	var yExtent = d3.extent(temperatures[0]);
	yExtent[1] = maxY;
	yExtent[0] = minY;
	var yRange = yExtent[1] - yExtent[0];

	// Adjust the lower and upper bounds to force the data
	// to fit into the y limits nicely
	yExtent[0] = yExtent[0] - yRange * 0.1;
	yExtent[1] = yExtent[1] + yRange * 0.1;

	// the y scale
	var yScale = d3.scale.linear()
		.range([chartHeight - margin.bottom, margin.top])
		.domain(yExtent);

	// x axis
	var xAxis = d3.svg.axis()
		.orient('bottom')
		.outerTickSize(0)
		.innerTickSize(0)
		.scale(xScale);

	// y axis
	var yAxis = d3.svg.axis()
		.orient('left')
		.outerTickSize(0)
		.innerTickSize(- (chartWidth - margin.left - margin.right))  // trick for creating quick gridlines
		.scale(yScale);

	var yAxis2 = d3.svg.axis()
		.orient('right')
		.outerTickSize(0)
		.innerTickSize(0)
		.scale(yScale);

	// Add the x axis to the chart
	var xAxisEl = axes.append('g')
		.attr('class', 'x-axis')
		.attr('transform', 'translate(' + 0 + ',' + (chartHeight - margin.bottom) + ')')
		.call(xAxis);

	// Add the y axis to the chart
	var yAxisEl = axes.append('g')
		.attr('class', 'y-axis')
		.attr('transform', 'translate(' + margin.left + ',' + 0 + ')')
		.call(yAxis);

	// Add the y axis to the chart
	var yAxisEl2 = axes.append('g')
		.attr('class', 'y-axis right')
		.attr('transform', 'translate(' + (chartWidth - margin.right) + ',' + 0 + ')')
		.call(yAxis2);

	// Format y-axis gridlines
	yAxisEl.selectAll('line')
		.style('stroke', '#BBB')
		.style('stroke-width', '1px')
		.style('shape-rendering', 'crispEdges');


	// Start data as a flat line at the average
	var avgTempY = yScale(d3.mean(temps));

	// Series container element
	var series = chart.append('g');

  console.log("Start draw svg path");
	var colors = ['red', 'blue', 'gold', 'green', 'fuchsia', 'orange', 'teal', 'purple', 'brown', 'violet'];
	for(var k=0;k<cards.length;k++) {
		var pathGenerator = d3.svg.line()
			.x(function(d, i) { return xScale(dates[i]); })
			.y(function(d, i) { return yScale(temperatures[k][i]); });
		series.append('path')
			.attr('vector-effect', 'non-scaling-stroke')
			.style('fill', 'none')
			.style('stroke', colors[k])
			.style('stroke-width', '1px')
			.attr('d', pathGenerator(dates));
	}

	// show latest max temperature
	var current = [];
	for(var j=0;j<cards.length;j++) {
		var last = temperatures.length-1;
		current.push(temperatures[j][last]);
	}
	var latest = d3.extent(current);
	console.log("max:"+latest[1]);
	d3.select("#current").html("Latest max: "+latest[1]+"°C");

	// add legend
	console.log("Adding legend");
	//var legend = document.querySelector("#legend");
	var legend = "<ul style='list-style:none'>";
	for(var k=0;k<cards.length;k++) {
		//var line = document.createTextNode(cards[k]+"<br/>");
		//legend.appendChild(line);
		legend += "<li><span style='background:"+colors[k]+";color:white;'>"+cards[k]+"</span></li>";
	}
	legend += "</ul>";
	d3.select("#legend").html(legend);

	// Add zooming and panning functionality, only along the x axis
	var zoom = d3.behavior.zoom()
		.scaleExtent([1, 12])
		.x(xScale)
		.on('zoom', function zoomHandler() {

			axes.select('.x-axis')
				.call(xAxis);

			series.attr('transform', 'translate(' + d3.event.translate[0] + ',0) scale(' + d3.event.scale + ',1)');

		});

	// The backRect captures zoom/pan events
	backRect.call(zoom);


	// Function for resetting any scaling and translation applied
	// during zooming and panning. Returns chart to original state.
	function resetZoom() {

		zoom.scale(1);
		zoom.translate([0, 0]);

		// Set x scale domain to the full data range
		xScale.domain(d3.extent(dates));

		// Update the x axis elements to match
		axes.select('.x-axis')
			.transition()
			.call(xAxis);

		// Remove any transformations applied to series elements
		series.transition()
			.attr('transform', "translate(0,0) scale(1,1)");

	};

	// Call resetZoom function when the button is clicked
	d3.select("#reset-zoom").on("click", resetZoom);




	// Active point element
	var activePoint = svg.append('circle')
		.attr({
			cx: 0,
			cy: 0,
			r: 5,
			'pointer-events': 'none'
		})
		.style({
			stroke: 'none',
			fill: 'red',
			'fill-opacity': 0
		});


	// Set container to have relative positioning. This allows us to easily
	// position the tooltip element with absolute positioning.
	container.style('position', 'relative');

	// Create the tooltip element. Hidden initially.
	var tt = container.append('div')
		.style({padding: '5px',
			border: '1px solid #AAA',
			color: 'black',
			position: 'absolute',
			visibility: 'hidden',
			'background-color': '#F5F5F5'
		});



	// Function for hiding the tooltip
	function hideTooltip() {

		tt.style('visibility', 'hidden');
		activePoint.style('fill-opacity', 0);

	}


	// Function for showing the tooltip
	function showTooltip() {

		tt.style('visibility', 'visible');
		activePoint.style('fill-opacity', 1);

	}


	// Tooltip content formatting function
	// function tooltipFormatter(date, temp) {
	// 	var dateFormat = d3.time.format('%d %b %Y %H:%M');
	// 	return dateFormat(date) + '<br><b>' + temp.toFixed(1) + '°C';
	// }
	function tooltipFormatter(date, temps) {
		var output = "";
		var dateFormat = d3.time.format('%d %b %Y %H:%M');
		output += dateFormat(date) + "<br/>";
		output += "<ol>";
		var range = d3.extent(temps);
		console.log("max:"+range[1]);
		for(var i=0;i<temps.length;i++) {
			if(temps[i] == range[1]) {
				output += "<li><b>"+ temps[i].toFixed(1) + "°C" + '</b></li>';
			} else {
				output += "<li>"+ temps[i].toFixed(1) + "°C" + '</li>';
			}
		}
		output += "</ol>";
		return output;
	}



	backRect.on('mousemove', function() {

		// Coords of mousemove event relative to the container div
		var coords = d3.mouse(container.node());

		// Value on the x scale corresponding to this location
		var xVal = xScale.invert(coords[0]);

		// TODO: 2. Make sure the interval is same in input collection
		// Date object corresponding the this x value. Add 1 hours to
		// the value, so that each point occurs at midday on the given date.
		// This means date changes occur exactly halfway between points.
		// This is what we want - we want our tooltip to display data for the
		// closest point to the mouse cursor.
		// we only care about hour every 5 minutes data point
		var d = new Date(xVal.getTime());
		var minutes = roundUpToFive(d.getMinutes());
		d.setMinutes(minutes);
		d.setSeconds(0);
		d.setMilliseconds(0);

		// Format the date object as a date string matching our original data
		var date = timeFormat(d);

		// Find the index of this date in the array of original date strings
		var i = dateStrings.indexOf(date);
		//console.log(date+" at index "+i);


		// Does this date exist in the original data?
		var dateExists = i > -1;

		// If not, hide the tooltip and return from this function
		if (!dateExists) {
			hideTooltip();
			return;
		}

		// If we are here, the date was found in the original data.
		// Proceed with displaying tooltip for of the i-th data point.

		// Get the i-th date value and temperature value.
		var _date = dates[i];
		var _temp = [];
		for(var j=0;j<cards.length;j++) {
			_temp.push(temperatures[j][i]);
		}

		// Update the position of the activePoint element
		var range = d3.extent(_temp);
		var max = _temp.indexOf(range[1]);
		activePoint.attr({
			cx: xScale(_date),
			cy: yScale(_temp[max])
		});

		// Update tooltip content
		tt.html(tooltipFormatter(_date, _temp));

		// Get dimensions of tooltip element
		var dim = tt.node().getBoundingClientRect();

		// Update the position of the tooltip. By default, above and to the right
		// of the mouse cursor.
		var tt_top = coords[1] - dim.height - 10,
		    tt_left = coords[0] + 10;

		// If right edge of tooltip goes beyond chart container, force it to move
		// to the left of the mouse cursor.
		if (tt_left + dim.width > chartWidth)
			tt_left = coords[0] - dim.width - 10;

		tt.style({
			top: tt_top + 'px',
			left: tt_left + 'px'
		});

		// Show tooltip if it is not already visible
		if (tt.style('visibility') != 'visible')
			showTooltip();

	});


	// Add mouseout event handler
	backRect.on('mouseout', hideTooltip);

}

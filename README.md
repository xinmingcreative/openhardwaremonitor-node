# OpenHardwareMonitor Node
NodeJs web server to display chart read from OpenHardwareMonitor log.

![screen](screenshot.PNG)

## TODO
1. [ ] Show CPU utilization chart.
2. [ ] Show memory chart.
3. [ ] Write unit test.
4. [ ] Integrate with gitlab-ci.
5. [ ] Make it by default is hourly view. When zoom in only start go into 5 minutes view.
6. [ ] Support dark and light theme.
7. [X] Read local csv file.
8. [X] Auto look up the latest data file in a folder.
9. [X] Add multi series to d3 linear chart.
10. [X] Accumulate days record.
11. [X] Add legend.
12. [X] Mouse over show data point correctly on series.
13. [ ] ~~Remove first header of OpenHardwareMonitor log.~~
14. [ ] ~~Refresh new data point by retrieve from a local csv file. Or possible refresh ``npm start`` every 5min?~~ see
 - https://stackoverflow.com/questions/1972242/how-to-auto-reload-files-in-node-js
 - https://codeburst.io/dont-use-nodemon-there-are-better-ways-fc016b50b45e


## Requirements
- [NodeJs](https://nodejs.org/en/).
- [d3](https://d3js.org/).
- [OpenHardwareMonitor](http://openhardwaremonitor.org/) > Enable logging to generate the *.csv files used as source input for this application.

## Setup
Map ``\public\data`` to source directory at local.

``mklink /D data C:\OpenHardwareMonitor\``

## Deploy
```
npm install
cd app
npm start
```

## Debug
```
set DEBUG=app:* & npm start
```

## How to Send Notification to Gitter
1. Obtain your token from https://developer.gitter.im/apps.
2. Add ``config.json`` file at root.
3. Install dependencies.
4. Run ``gitter.js``.

[config.json]
```json
{
	"gitter": "gitter_api_token",
	"eth": "ETH_account",
	"sia": "Sia_account",
	"host": "smtp.gmail.com",
	"email": "your@gmail.com",
	"password": "email_password"
}
```

```
npm install node-gitter --save
npm install request --save
node gitter.js
```

Have fun!

## How to Send Email
```
npm install nodemailer --save
node sendmail.js
```

## Start a Simple Web Server
```
node index.js
```
Then load http://localhost:8888/ in your browser to access the app.

Modified from https://stackoverflow.com/a/15231979/707752


## Setup Express
```
mkdir app
cd app
npm init
npm install express --save
```

## Initial Express Application
Using Express Application Generator
```
npm install express-generator -g
express --view=pug app
cd app
npm install
```

## How to Create A Symlink in Windows
- Create a symlink to a file ``mklink temperature.csv ..\..\..\temperature.csv``
- Create a symlink to a folder ``mklink /D BingImages C:\Users\Pictures\BingImages``

see [Symbolic Link](https://en.wikipedia.org/wiki/Symbolic_link) on wiki.


## References
- [Node.js](http://nodejs.org)
- [d3](https://d3js.org/)
- [d3.csv](https://github.com/d3/d3-request/blob/master/README.md#csv)
- [Expressjs](http://expressjs.com)
- [Pubjs](https://pugjs.org/)
- [Javascript Date Functions](https://www.w3schools.com/jsref/jsref_obj_date.asp)
- [Creating basic site with Nodejs and Express](http://shapeshed.com/creating-a-basic-site-with-node-and-express/)
- [Create Simple Line Graph Using d3](http://www.d3noob.org/2016/08/create-simple-line-graph-using-d3js-v4.html)
- [Add Interactive chart with tooltip](http://davidbanks.co.nz/post/building-interactive-charts-with-d3)
- [TAMBUN-1070 Archive Temperature](https://docs.google.com/spreadsheets/d/e/2PACX-1vTwmIaxw2nDoWrY9PRib3l3BzBMJqBM22smeNsw-BCqPlqYE8lV2yruS9cGbESOV6wW72mmQIrhtkpa/pubchart?oid=2056329689&format=interactive)
- [TAMBUN-1060x8 Archive Temperature](https://docs.google.com/spreadsheets/d/e/2PACX-1vS4U4yhSPNu5QOu9akolpzuzkr9K5MnJTPt0Y4vRMrz5ibNODwmlv2ksgxXXdBXB_ULs3fGihjXInPb/pubchart?oid=2066678219&format=interactive)
- [node-gitter](https://www.npmjs.com/package/node-gitter)
